<?php

Route::namespace('Api')->group(function () {

    Route::namespace('Auth')->prefix('auth')->name('auth.')->group(function () {
        Route::post('login', 'LoginController@login')->name('login');
    });

    Route::middleware('jwt.auth')->namespace('Employee')->prefix('employees')->name('employees.')->group(function () {
        Route::get('', 'EmployeeController@index')->name('index');
        Route::post('csv', 'EmployeeController@csv')->name('csv');
        Route::delete('{document}', 'EmployeeController@delete')->name('delete');
    });

});
