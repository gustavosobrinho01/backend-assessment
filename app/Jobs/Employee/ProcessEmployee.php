<?php

namespace App\Jobs\Employee;

use App\Employee;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessEmployee implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $manager;

    /**
     * Create a new job instance.
     *
     * @param array $data
     * @param User $manager
     */
    public function __construct(array $data, User $manager)
    {
        $this->data = $data;
        $this->manager = $manager;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $employee = $this->getEmployee();
        $employee->manager_id = $this->manager->id;
        $employee->fill($this->data);
        $employee->save();
    }

    private function getEmployee()
    {
        return $this->manager->employees()
                ->where('email', $this->data['email'])
                ->where('document', $this->data['document'])
                ->first() ?? new Employee();
    }
}
