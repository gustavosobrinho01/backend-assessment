<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EmployeeCsvUploaded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $fileFullPath;

    /**
     * Create a new event instance.
     *
     * @param string $fileFullPath
     */
    public function __construct(string $fileFullPath)
    {
        $this->fileFullPath = $fileFullPath;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
