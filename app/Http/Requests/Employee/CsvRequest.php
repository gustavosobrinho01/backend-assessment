<?php

namespace App\Http\Requests\Employee;

use App\Rules\ExtensionRule;
use Illuminate\Foundation\Http\FormRequest;

class CsvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employees' => [
                'required',
                'mimes:text/csv,csv,txt',
                new ExtensionRule('csv')
            ]
        ];
    }
}
