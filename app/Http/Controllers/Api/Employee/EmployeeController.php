<?php

namespace App\Http\Controllers\Api\Employee;

use App\Employee;
use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\CsvRequest;
use App\Services\Csv\CsvService;
use Symfony\Component\HttpFoundation\Response;

class EmployeeController extends Controller
{
    protected $csvService;

    public function __construct(CsvService $csvService)
    {
        $this->csvService = $csvService;
    }

    public function index()
    {
        $employees = auth()->user()->employees;

        return response()->json($employees, Response::HTTP_OK);
    }

    public function csv(CsvRequest $request)
    {
        $this->csvService->store($request);

        return response()->json([
            'CSV added to the queue for processing, you will receive an email informing you that the upload has been completed.'
        ], Response::HTTP_CREATED);
    }

    public function delete(string $document)
    {
        $employee = Employee::where('document', $document)->firstOrFail();

        $this->authorize('delete', $employee);

        $employee->delete();

        return response()->noContent();
    }
}
