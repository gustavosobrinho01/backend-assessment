<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use Uuid;

    protected $keyType = 'string';

    protected $fillable = [
        'name',
        'email',
        'document',
        'city',
        'state',
        'start_date',
    ];

    // relations

    public function manager()
    {
        return $this->hasMany(Employee::class, 'manager_id', 'id');
    }
}
