<?php

namespace App\Services\Csv;

use App\Employee;
use App\Events\EmployeeCsvUploaded;
use App\Http\Requests\Employee\CsvRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class CsvService
{
    /**
     * Save uploaded file
     * @param CsvRequest $request
     * @return string
     */
    public function store(CsvRequest $request): string
    {
        $employees = $request->employees;

        $filePath = $this->getFilePath();
        $fileName = $this->getFileName($employees);

        $fileFullPath = Storage::putFileAs($filePath, $employees, $fileName);

        event(new EmployeeCsvUploaded($fileFullPath));

        return $fileFullPath;
    }

    private function getFileName(UploadedFile $employees): string
    {
        return now()->format('Y_m_d_H_i_s') . '-' . $employees->getClientOriginalName();
    }

    private function getFilePath(): string
    {
        return auth()->user()->id . "/csv/uploaded/";
    }
}
