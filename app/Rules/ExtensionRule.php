<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ExtensionRule implements Rule
{
    protected $extension;

    /**
     * Create a new rule instance.
     *
     * @param string $extension
     */
    public function __construct(string $extension)
    {
        $this->extension = $extension;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $fileName = explode('.', $value->getClientOriginalName());

        return end($fileName) === $this->extension;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a file of extension: :values.';
    }
}
