<?php

namespace App\Listeners;

use App\Jobs\Employee\ProcessEmployee;
use App\Mail\CsvProcessed;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use League\Csv\Exception;
use League\Csv\Reader;

class DispatchEmployeeCsvUploaded
{
    protected $manager;

    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        $this->manager = auth()->user();
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     * @throws Exception
     */
    public function handle($event)
    {
        $fileFullPath = $event->fileFullPath;

        $csv = Reader::createFromString(Storage::get($fileFullPath));
        $csv->setHeaderOffset(0);

        $employees = $csv->getRecords();

        foreach ($employees as $employee) {
            ProcessEmployee::dispatchNow($employee, auth()->user());
        }

        Mail::to($this->manager)->send(new CsvProcessed());
    }
}
